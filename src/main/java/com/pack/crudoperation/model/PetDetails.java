package com.pack.crudoperation.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name="pets")
public class PetDetails implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@Id
	private Integer id;
	@Column
    private String petname;
	@Column
    private Integer age;
	@Column
    private String placefrom;
	@Column
    private String cost;
	
	@Column(name = "PetCount", nullable = false, columnDefinition = "int default 0") 
	private Integer petcount;
	public PetDetails(Integer id, String petname, Integer age, String placefrom, String cost) {
		super();
		this.id = id;
		this.petname = petname;
		this.age = age;
		this.placefrom = placefrom;
		this.cost = cost;
		
	}
   
	public PetDetails() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getPetname() {
		return petname;
	}


	public void setPetname(String petname) {
		this.petname = petname;
	}


	public Integer getage() {
		return age;
	}


	public void setage(Integer age) {
		this.age = age;
	}


	public String getPlacefrom() {
		return placefrom;
	}


	public void setPlacefrom(String placefrom) {
		this.placefrom = placefrom;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public Integer getPetcount() {
		return petcount;
	}


	public void setPetcount(Integer petcount) {
		this.petcount = petcount;
	}
	
	
	
       
}