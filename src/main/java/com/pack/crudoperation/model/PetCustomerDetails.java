package com.pack.crudoperation.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name="PetCustomer")
public class PetCustomerDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	@NotEmpty(message="{firstName.not.empty}")
	@Size(min=5,max=12,message="{Firstname.size}")
	private String firstName;
	@NotEmpty(message="{phoneNumber.not.empty}")
	@Size(min=10,message="{Phonenumber.size}")
	private String phoneNumber;
	@NotEmpty(message="{email.not.empty}")
	@Email(message="{email.email}")
	private String email;
	@NotEmpty(message="{country.not.empty}")
	private String country;
	@NotEmpty(message= "{passWord.not.empty}")
    @Size(min=7,max=12,message="{Password.size}")
	private String passWord;
	
	
	
	
	
	public PetCustomerDetails(Integer id, String firstName, String phoneNumber, String email, String country,
			String passWord) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.country = country;
		this.passWord = passWord;
	}
	public PetCustomerDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "PetCustomerDetails [id=" + id + ", firstName=" + firstName + ", phoneNumber=" + phoneNumber + ", email="
				+ email + ", country=" + country + ", passWord=" + passWord + "]";
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	
}
