<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Registration</title>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"  rel="stylesheet">

<style>
.has-error
{
color:red;
}
</style>
</head>
<body>
<div class="container-fluid">
<h2>Customer Registration</h2>
   <form:form action="save" method="post" Class="form-horizontal" modelAttribute="petCustomerDetails"  novalidate="novalidate">
    <div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="FirstName">First name</label>
            <div class="col-md-7">
              <form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
              <div class ="has-error">
              <form:errors path ="firstName" class="help-line" />
              </div>
            </div>

</div>
</div>

<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="PhoneNumber">Phone Number</label>
            <div class="col-md-7">
              <form:input type="text" path="phoneNumber" id="PhoneNumber" class="form-control input-sm"/>
            <div class ="has-error">
              <form:errors path ="phoneNumber" class="help-line" />
              </div>
            </div>

</div>
</div>

<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="email">Email</label>
            <div class="col-md-7">
            <form:input type="text" path="email" id="email" class="form-control input-sm"/>
            <div class ="has-error">
              <form:errors path ="email" class="help-line" />
              </div>
            </div>

</div>
</div>

<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="country">Country</label>
            <div class="col-md-7" >
              <form:input  path="country" id="country" class="form-control input-sm" />
             
              <div class ="has-error">
              <form:errors path ="country" class="help-line" />
              </div>
            </div>

</div>
</div>


<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="PassWord">Password</label>
            <div class="col-md-7">
            <form:input type="text" path="passWord" id="Password" class="form-control input-sm"/>
            <div class ="has-error">
              <form:errors path ="PassWord" class="help-line" />
              </div>
            </div>

</div>
</div>

<div class="row">
         <div class="form-actions floatRight">
            <input type="submit" value="Register" Class="btn btn-primary btn-sm">
            
           
          
            </div>

</div>


</form:form>
</div>

</body>
</html>