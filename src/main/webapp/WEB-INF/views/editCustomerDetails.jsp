<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit Customer Details</title>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"  rel="stylesheet">
<style>
.has-error
{
color:red;
}
</style>
</head>
<body>
<div class="container-fluid">
<h2>Customer Information </h2>
<c:url var="action" value="/update" />
   <form:form action="${action}" method="post" Class="form-horizontal" modelAttribute="editCustomerDetails" novalidate="novalidate">
   
      <div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="id">Id</label>
            <div class="col-md-7">
              <form:input type="text" path="id" id="id" class="form-control input-sm" readonly="readonly"/>
              <div class ="has-error">
              <form:errors path ="id" class="help-line" />
              </div>
            </div>

</div>
</div>
   
   
    <div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="firstName">First name</label>
            <div class="col-md-7">
              <form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
              <div class ="has-error">
              <form:errors path ="firstName" class="help-line" />
              </div>
            </div>

</div>
</div>

<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="PhoneNumber">Phone Number</label>
            <div class="col-md-7">
              <form:input type="text" path="PhoneNumber" id="PhoneNumber" class="form-control input-sm"/>
            <div class ="has-error">
              <form:errors path ="PhoneNumber" class="help-line" />
              </div>
            </div>

</div>
</div>

<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="email">Email</label>
            <div class="col-md-7">
              <form:input type="text" path="email" id="email" class="form-control input-sm"/>
              <div class ="has-error">
              <form:errors path ="email" class="help-line" />
              </div>
            </div>

</div>
</div>



<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="country">Country</label>
            <div class="col-md-7" >
              <form:input path="country" id="country" class="form-control input-sm" />
            
              <div class ="has-error">
              <form:errors path ="country" class="help-line" />
              </div>
            </div>

</div>
</div>



<div class="row">
         <div class="form-group col-md-12">
            <label class="col-md-3 control-table" for="PassWord"> Password </label>
            <div class="col-md-7">
              <form:input type="text" path="email" id="PassWord" class="form-control input-sm" readonly="readonly" />
                
              <div class ="has-error">
              <form:errors path ="PassWord" class="help-line" />
              </div>
            </div>

</div>
</div>



<div class="row">
         <div class="form-actions floatRight">
            <input type="submit" value="Register" Class="btn btn-primary btn-sm">
            
           
          
            </div>

</div>
</form:form>
</div>

</body>
</html>