<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer List </title>


<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"  rel="stylesheet">
</head>
<body>
<h1 align="centre">Customer Details</h1>
<table celloading="2">
<tr>
<th>
<a href="/register"><h2>register new customer</h2></a>
</th>

<th>
<a align="right" href="/delete"><h2>Delete All Customers</h2></a>
</th>
</tr>
</table>



<form:form  class="form-horizontal" >
<table  border="2" width="70%" cellpadding="2">
<tr><th>Id</th>
<th>First Name</th>
<th>Phone Number</th>
<th>Email</th>
<th>Country</th>
<th>Edit</th><th>Delete</th></tr>  

 

   <c:forEach var="customer" items="${list}"> 
   <tr>  
   <td>${customer.id}</td>  
   <td>${customer.firstName}</td> 
   <td>${customer.phoneNumber}</td>  
   <td>${customer.email}</td>
   <td>${customer.country}</td>  
  
   <td><a href="/editcustomer/${customer.id}">Edit</a></td>  
   <td><a href="/deletecustomer/${customer.id}">Delete</a></td>  
   </tr>  
   </c:forEach> 
   
   
   </table>  
   <br/>
   
  
</form:form>


</body>
</html>